import java.util.HashSet;

public class Main {
    private static void attack() {
        Generator generator = new Generator(22, new String[]{"at", "bU", "c6"});
        doWithGenerator(generator);
    }

    private static void normal() {
        Generator generator = new Generator(22, new String[]{"ab", "cd", "ef"});
        doWithGenerator(generator);
    }

    private static void doWithGenerator(Generator generator) {
        HashSet<String> set = new HashSet<>();

        String s;
        long start = System.currentTimeMillis();
        while ((s = generator.next()) != null) ;
        long end = System.currentTimeMillis();
        System.out.println("Plain: " + (end - start) + "ms");

        generator.rewind();
        start = System.currentTimeMillis();
        while ((s = generator.next()) != null) {
            set.add(s);
        }
        end = System.currentTimeMillis();
        System.out.println("Add operation: " + (end - start) + "ms");

        generator.rewind();
        start = System.currentTimeMillis();
        while ((s = generator.next()) != null) {
            if (!set.contains(s))
                System.exit(1);
        }
        end = System.currentTimeMillis();
        System.out.println("Contains operation: " + (end - start) + "ms");
    }

    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println("Iteration " + i);
            attack();
            System.out.println();
        }


        for (int i = 1; i <= 10; i++) {
            System.out.println("Iteration " + i);
            normal();
            System.out.println();
        }
    }
}
