public class Generator {
    private String[] base;
    private int length;
    private int counter;

    public Generator(int length, String[] base) {
        this.base = base;
        this.length = length;
        rewind();
    }

    public void rewind() {
        this.counter = 0;
    }

    public String next() {
        StringBuilder sb = new StringBuilder();
        int idx = this.counter++;
        while (sb.length() < this.length) {
            sb.append(base[idx % base.length]);
            idx /= base.length;
        }

        if (idx != 0)
            return null;
        return sb.toString();
    }
}
