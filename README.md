# Hash Attack Result

## String Length = 22 

Total number: 3^11 = 177147

Same hashCode:

```
Iteration 1
Plain: 53ms
Add operation: 213ms
Contains operation: 181ms

Iteration 2
Plain: 47ms
Add operation: 179ms
Contains operation: 164ms

Iteration 3
Plain: 39ms
Add operation: 132ms
Contains operation: 142ms

Iteration 4
Plain: 49ms
Add operation: 126ms
Contains operation: 103ms

Iteration 5
Plain: 42ms
Add operation: 137ms
Contains operation: 115ms

Iteration 6
Plain: 50ms
Add operation: 120ms
Contains operation: 103ms

Iteration 7
Plain: 38ms
Add operation: 119ms
Contains operation: 103ms

Iteration 8
Plain: 50ms
Add operation: 121ms
Contains operation: 103ms

Iteration 9
Plain: 37ms
Add operation: 119ms
Contains operation: 102ms

Iteration 10
Plain: 41ms
Add operation: 135ms
Contains operation: 114ms
```

Different hashCode:

```
Iteration 1
Plain: 43ms
Add operation: 86ms
Contains operation: 86ms

Iteration 2
Plain: 50ms
Add operation: 71ms
Contains operation: 60ms

Iteration 3
Plain: 34ms
Add operation: 74ms
Contains operation: 68ms

Iteration 4
Plain: 34ms
Add operation: 73ms
Contains operation: 68ms

Iteration 5
Plain: 33ms
Add operation: 66ms
Contains operation: 64ms

Iteration 6
Plain: 36ms
Add operation: 69ms
Contains operation: 65ms

Iteration 7
Plain: 36ms
Add operation: 80ms
Contains operation: 69ms

Iteration 8
Plain: 33ms
Add operation: 73ms
Contains operation: 69ms

Iteration 9
Plain: 34ms
Add operation: 67ms
Contains operation: 70ms

Iteration 10
Plain: 35ms
Add operation: 81ms
Contains operation: 72ms
```

## String Length = 28

Total number = 3^14 = 4782969

Same hashCode:

```
Iteration 1
Plain: 1207ms
Add operation: 7948ms
Contains operation: 8104ms

Iteration 2
Plain: 1233ms
Add operation: 7136ms
Contains operation: 7543ms

Iteration 3
Plain: 1068ms
Add operation: 7190ms
Contains operation: 7580ms

Iteration 4
Plain: 1063ms
Add operation: 7318ms
Contains operation: 7684ms

Iteration 5
Plain: 1067ms
Add operation: 6786ms
Contains operation: 7769ms

Iteration 6
Plain: 1063ms
Add operation: 7025ms
Contains operation: 7573ms

Iteration 7
Plain: 1061ms
Add operation: 7234ms
Contains operation: 7756ms

Iteration 8
Plain: 1060ms
Add operation: 6739ms
Contains operation: 7722ms

Iteration 9
Plain: 1065ms
Add operation: 7075ms
Contains operation: 7728ms

Iteration 10
Plain: 1059ms
Add operation: 6825ms
Contains operation: 7611ms
```

Different hashCode:

```
Iteration 1
Plain: 1063ms
Add operation: 5513ms
Contains operation: 4546ms

Iteration 2
Plain: 1079ms
Add operation: 5973ms
Contains operation: 4549ms

Iteration 3
Plain: 1085ms
Add operation: 5533ms
Contains operation: 5262ms

Iteration 4
Plain: 1069ms
Add operation: 5369ms
Contains operation: 4249ms

Iteration 5
Plain: 1070ms
Add operation: 6111ms
Contains operation: 4532ms

Iteration 6
Plain: 1078ms
Add operation: 5530ms
Contains operation: 4600ms

Iteration 7
Plain: 1085ms
Add operation: 6155ms
Contains operation: 4425ms

Iteration 8
Plain: 1074ms
Add operation: 5431ms
Contains operation: 4471ms

Iteration 9
Plain: 1067ms
Add operation: 6285ms
Contains operation: 4434ms

Iteration 10
Plain: 1081ms
Add operation: 5426ms
Contains operation: 4532ms
```